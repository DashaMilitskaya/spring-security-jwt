package ru.csu.springsecurity.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import ru.csu.springsecurity.dto.request.LoginRequest;
import ru.csu.springsecurity.dto.request.RegRequest;
import ru.csu.springsecurity.dto.response.JwtResponse;
import ru.csu.springsecurity.service.impl.SecurityService;

@RestController
@RequiredArgsConstructor
public class RegController {

    private final SecurityService securityService;

    @PostMapping("/reg")
    @ResponseBody
    public JwtResponse registration(@RequestBody RegRequest regRequest) {
        return securityService.registration(regRequest);
    }

}