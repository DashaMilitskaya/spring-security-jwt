package ru.csu.springsecurity.dto.request;

import lombok.Value;
@Value
public class RegRequest {
    String username;
    String password;
    String email;
}
