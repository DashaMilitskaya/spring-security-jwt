package ru.csu.springsecurity.dto.response;

import lombok.Builder;
import lombok.Data;

import java.util.Set;

@Data
@Builder
public class JwtResponse {
    private String token;
    private String type;
    private String username;
    private String email;
    private Set<String> roles;
}
