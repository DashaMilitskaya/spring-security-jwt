package ru.csu.springsecurity.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import ru.csu.springsecurity.dao.entity.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import ru.csu.springsecurity.dao.repository.UserRepository;
import ru.csu.springsecurity.dto.request.LoginRequest;
import ru.csu.springsecurity.dto.request.RegRequest;
import ru.csu.springsecurity.dto.response.JwtResponse;
import ru.csu.springsecurity.jwt.JwtUtils;

import java.util.Set;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
@Log4j2
public class SecurityService {

    private final AuthenticationManager authenticationManager;
    private final JwtUtils jwtUtils;
    private final PasswordEncoder encoder;
    private final UserRepository userRepository;
    public JwtResponse login(LoginRequest request) {
        log.info(encoder.encode(request.getPassword()));
        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();

        Set<String> roles = userDetails.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toSet());
        String jwt = jwtUtils.generateJwtToken(authentication);


        return JwtResponse
                .builder()
                .roles(roles)
                .username(userDetails.getUsername())
                .token(jwt)
                .build();
    }
    public JwtResponse registration(RegRequest request){
        log.info(request.getUsername() + request.getEmail() + encoder.encode(request.getPassword()));


        User user = User.builder()
                .email(request.getEmail())
                .username(request.getUsername())
                .password(encoder.encode(request.getPassword()))
                .build();

        userRepository.save(user);

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();

        Set<String> roles = userDetails.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toSet());
        String jwt = jwtUtils.generateJwtToken(authentication);


        return JwtResponse
                .builder()
                .roles(roles)
                .username(userDetails.getUsername())
                .token(jwt)
                .build();
    }
}
